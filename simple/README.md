# Outline

## Cross Compiling Simple programs, phase wise
```
arm-linux-gnueabi-gcc   hello.c -E                 
arm-linux-gnueabi-cpp   hello.c           
arm-linux-gnueabi-gcc   hello.c -c          
arm-linux-gnueabi-gcc   hello.o -o h1.out    
```

## Testing the cross binary
On Host

```
sudo mount rootfs.img /mnt/rootfs           # where rootfs.img exists
sudo cp h1.out /mnt/rootfs/home/root        # where h1.out exists
sudo umount /mnt/rootfs
```
On Target
```
./h1.out
```

## Analysis
On Host
```
file h1.out
arm-linux-gnueabi-readelf -h h1.out
```

## Compatibility check
```
arm-linux-gnueabihf-gcc hello.c -o h2.out
#copy h2.out to rootfs and test the execution
```
# do h2.out execute on target rootfs ??
``` 
file h1.out
arm-linux-gnueabi-readelf -h h1.out
file /mnt/rootfs/lib/libc-2.27.so 
arm-linux-gnueabi-readelf -h libc-2.27.so 
# Observe the flags soft-float or hard-float
```




